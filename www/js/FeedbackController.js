angular
	.module('starter')
	.controller('FeedbackController', function($scope, $location, $http) {
		$scope.title = "Q-Venient"

		$scope.feedback = {};

		$scope.cdate = new Date();

		$scope.sendfeedback = function() {
			console.log($scope.feedback);
			console.log($scope.cdate)
			var data = {
				message: $scope.feedback.message, date_created: $scope.cdate
			}

			$http.post('http://localhost:1234/myApp/feedback', data)
				.success(function (data, successFeed) {
					console.log(successFeed)

					alert("Your Feedback has been sent");

					$scope.clearAll();
				})
		}

		$scope.clearAll = function() {
			$scope.feedback = "";
		}
	})