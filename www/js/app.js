// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
<<<<<<< HEAD
angular.module('starter', ['ionic', 'starter.controllers'])
=======
angular.module('starter', ['ionic', 'starter.controllers', 'ngCookies'])
>>>>>>> e5170d2600f53e50743c826b7f04f86ad830c270

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)
      cordova.plugins.Keyboard.disableScroll(true)

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault()
    }
  })
})

.value('Request', null)

.config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider

    .state('qmsf', {
    url: '/qmsf',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'LoginController'
  })
    .state('qmsf.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeController'
        }
      }
    })
    .state('qmsf.aboutus', {
      url: '/about_us',
      views: {
        'menuContent': {
          templateUrl: 'templates/aboutus.html',
          controller: 'AboutController'
        }
      }
    })

// ----------------------------- Personnel -------------------------------- //

  .state('qmsf_personnel', {
    url: '/qmsf_personnel',
    abstract: true,
    templateUrl: 'templates/Personnel/menu.html',
    controller: 'PersonnelHomeController'
  })
    .state('qmsf_personnel.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/Personnel/profile.html',
          controller: 'ProfileController'
        }
      }
    })   
    .state('qmsf_personnel.home', {
      url: '/home',
      views: {
        'menuContent' : {
          templateUrl: 'templates/Personnel/home.html',
          controller: 'PersonnelHomeController'
        }
      }
    })
    .state('qmsf_personnel.aboutus', {
      url: '/about_us',
      views: {
        'menuContent': {
          templateUrl: 'templates/Personnel/aboutus.html',
          controller: 'AboutController'
        }
      }
<<<<<<< HEAD
    })    
=======
    })   
>>>>>>> e5170d2600f53e50743c826b7f04f86ad830c270


// ----------------------------- Customer ------------------------------------ //

  .state('qmsf_customer', {
    url: '/qmsf_customer',
    abstract: true,
    templateUrl: 'templates/Customer/menu.html',
    controller: 'CustomerHomeController'
  })
    .state('qmsf_customer.home', {
      url: '/home',
      views: {
        'menuContent' : {
          templateUrl: 'templates/Customer/home.html',
          controller: 'CustomerHomeController'
        }
      }
    })
    .state('qmsf_customer.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/profile.html',
          controller: 'ProfileController'
        }
      }
    })    
    .state('qmsf_customer.aboutus', {
      url: '/about_us',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/aboutus.html',
          controller: 'AboutController'
        }
      }
    })
    .state('qmsf_customer.queueNow', {
      url: '/queueNow',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/queueNow.html',
          controller: 'AboutController'
        }
      }
    })
<<<<<<< HEAD
    .state('qmsf_customer.queueInfo', {
      url: '/queueInfo',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/queueInfo.html',
          controller: 'AboutController'
        }
      }
    })

=======
    .state('qmsf_customer.feedback', {
      url: '/feedback',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/feedback.html',
          controller: 'FeedbackController'
        }
      }
    })    

// --------------------------- New Customer Menu -------------------------
  
  .state('qmsf_customer_new', {
    url: '/qmsf_customer_new',
    abstract: true,
    templateUrl: 'templates/Customer/new_menu.html',
    controller: 'CustomerHomeController'
  })
    .state('qmsf_customer_new.queueInfo', {
      url: '/queueInfo',
      views: {
        'menuContent' : {
          templateUrl: 'templates/Customer/queueInfo.html',
          controller: 'CustomerHomeController'
        }
      }
    })
    .state('qmsf_customer_new.profile', {
      url: '/profile',
      views: {
        'menuContent' : {
          templateUrl: 'templates/Customer/profile.html',
          controller: 'ProfileController'
        }
      }
    })
    .state('qmsf_customer_new.aboutus', {
      url: '/about_us',
      views: {
        'menuContent' : {
          templateUrl: 'templates/Customer/aboutus.html',
          controller: 'AboutController'
        }
      }
    })        
    .state('qmsf_customer_new.feedback', {
      url: '/feedback',
      views: {
        'menuContent': {
          templateUrl: 'templates/Customer/feedback.html',
          controller: 'FeedbackController'
        }
      }
    }) 


// ----------------------------- End sa tanan ----------------------------
>>>>>>> e5170d2600f53e50743c826b7f04f86ad830c270
    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('qmsf/home');
})
